const express = require('express');
const router = express.Router(); 
const alerts = require('../data/alerts');

//Get footer links
router.get('/alerts', (req, res) => {
    // console.log('TEST');
    // console.log(alerts.getAlerts());
    res.json({success: true, alerts: alerts.getAlerts()});
});


module.exports = router;