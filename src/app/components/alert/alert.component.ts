import { Component, OnInit, ElementRef } from '@angular/core';
import { AlertService } from '../../services/alert.service';
declare var $: any;

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css'],
  providers: [AlertService]
})
export class AlertComponent implements OnInit {
  private alerts: Array<Object>; private totalCount: number; private dom;
  private isDataFiltered: boolean; private severity_arr: Array<string>;
  private clientip_arr: Array<string>; private protocol_arr: Array<string>;
  private countries_arr: Array<string>;
  private severityBlockData: Array<Object>; private clientIPBlockData: Array<Object>;
  private protocolBlockData: Array<Object>; private countryBlockData: Array<Object>;

  constructor(private alertService: AlertService, private elRef: ElementRef) { }
  ngOnInit() {
    this.dom = $(this.elRef.nativeElement);
    this.isDataFiltered = false;
    this.retrieveAlerts();
  }

  retrieveAlerts() {
    this.alertService.getAlerts().subscribe(
      data => {
        this.alerts = data.alerts;
        this.processAllData(this.alerts);
      },
      err => {
        console.log(err);
      }
    );
  }

  processAllData(data) {
    this.totalCount = data.length;
    this.severity_arr = this.createUniquePropsValArray(this.alerts, 'Severity');
    this.clientip_arr = this.createUniquePropsValArray(this.alerts, 'ClientIP');
    this.protocol_arr = this.createUniquePropsValArray(this.alerts, 'Protocol');
    this.countries_arr = this.createUniquePropsValArray(this.alerts, 'ClientCountry');
    this.severityBlockData = this.processUniquePropsValArr(this.severity_arr, 'Severity');
    this.clientIPBlockData = this.processUniquePropsValArr(this.clientip_arr, 'ClientIP');
    this.protocolBlockData = this.processUniquePropsValArr(this.protocol_arr, 'Protocol');
    this.countryBlockData = this.processUniquePropsValArr(this.countries_arr, 'ClientCountry');
  }

  createUniquePropsValArray(data, key) {
    let flags = [], output = [], l = data.length, i;
    for (i = 0; i < l; i++) {
      if (flags[data[i][key]]) continue;
      flags[data[i][key]] = true;
      output.push(data[i][key]);
    }
    return output;
  }

  processUniquePropsValArr(array, field) {
    var arr = [];
    for (let i = 0; i < array.length; i++) {
      arr.push(this.countObjsByPropVal(field, array[i], this.alerts));
    }
    return arr;
  }

  countObjsByPropVal(key, val, array) {
    let count = 0; let arr = [], obj = {};
    for (let i = 0; i < array.length; i++) {
      if (array[i][key] == val) {
        count += 1;
        obj = {
          key: val,
          count: count
        }
      }
    }
    arr.push(obj);
    return obj;
  }

  filterData(e, val) {
    let field = e.target.className,
      filter = field + ' = ' + val;
    this.createFilterStatusEl(filter);
    this.isDataFiltered = true;
    let arr = [];
    for (let i = 0; i < this.alerts.length; i++) {
      if (this.alerts[i][field] == val) {
        arr.push(this.alerts[i]);
      }
    }
    this.alerts = arr;
    this.processAllData(this.alerts);
  }

  createFilterStatusEl(filter) {
    let span = document.createElement('span');
    span.classList.add('filter');
    let txtNode = document.createTextNode(filter);
    span.appendChild(txtNode);
    document.getElementById('filterDv').appendChild(span);
  }

  resetData() {
    this.retrieveAlerts();
    this.isDataFiltered = false;
    this.dom.find('.filter').hide();
  }
}