import { Routes } from '@angular/router';
import { AlertComponent } from './components/alert/alert.component';

export const appRoutes: Routes = [
    { path: '', component: AlertComponent }
];
