import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AlertService {
private alertsUrl = 'api/alerts';
  constructor(private http: Http) { }

  getAlerts() {
    return this.http.get(this.alertsUrl).map(data => data.json());
  }
}
