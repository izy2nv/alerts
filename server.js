const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const cors = require('cors');

//Set port
const port = process.env.PORT || '3000';
app.use(cors());
app.listen(port, () => {
    console.log('Server started on port: '+ port);
});

const api = require('./routes/api');

// Set the body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Point server to Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

//Set API file location
app.use('/api', api);

//Forward all other requests to the Angular index.html file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});